from flask import Flask, jsonify
import pandas as pd
from datetime import datetime, timedelta

app = Flask(__name__)

# Charger les données CSV
data = pd.read_csv('donnes_regionales_energies_2021.csv', delimiter=';', encoding='utf-8')
data['Date - Heure'] = pd.to_datetime(data['Date - Heure'], utc=True)  

#Déclaration de la période couverte, à faire évoluer au besoin de la simulation
current_time = datetime(2021, 1, 1, 0, 0)
end_time = datetime(2022, 1, 1, 0, 0)  

@app.route('/', methods=['GET'])
def get_data():
    global current_time
    global end_time

    if current_time <= end_time:
        # Filtrer les données pour la date et l'heure actuelles
        specific_time_data = data[data['Date - Heure'] == current_time.strftime('%Y-%m-%dT%H:%M:%S+01:00')]
        # Convertir les données au format JSON
        json_data = specific_time_data.to_json(orient='records', force_ascii=False)
        # Incrémenter la date d'une demi-heure à chaque appel
        current_time += timedelta(minutes=30)

        response = app.response_class(
            response=json_data,
            status=200,
            mimetype='application/json; charset=utf-8'
        )

    return response

if __name__ == '__main__':
    app.run(debug=True)
