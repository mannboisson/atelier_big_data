import time, json, random
from kafka import KafkaProducer
import requests
from datetime import datetime, timedelta
import pandas as pd
import pytz

def getDate(timestamp):
    #Correction du format erroné de la date 
    timestamp_str = str(timestamp)[:10]
    timestamp_unix = int(timestamp_str)
    # Convertir en format datetime en UTC
    current_date_utc = datetime.utcfromtimestamp(timestamp_unix).replace(tzinfo=pytz.utc)
    paris_timezone = pytz.timezone('Europe/Paris')

    return current_date_utc.astimezone(paris_timezone).isoformat()

# Adresse du serveur Flask
flask_server_url = 'http://localhost:5000'
# Adresse du producer
producteur = KafkaProducer(bootstrap_servers='localhost:9092')

while True:
    # Appel du serveur Flask pour obtenir les données au format JSON
    response = requests.get(flask_server_url)
    data_json = response.json()

    # Lecture des valeurs
    for entry in data_json:
        

        current_date = getDate(entry['Date - Heure'])
        
        code_insee = entry['Code INSEE région']
        region_name = entry['Région']
        consumption = entry['Consommation (MW)']

        # Construction du message souhaité
        msg = {
            'name': region_name,
            'consumption': consumption,
            'date': current_date
        }

        print(current_date)
        producteur.send("geographique", json.dumps(msg).encode(), json.dumps(code_insee).encode())
        time.sleep(0.1)

