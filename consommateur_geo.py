import json
from kafka import KafkaConsumer

keys = []
mem = {c: 0 for c in keys}
consommateur = KafkaConsumer('geographique', group_id='geographique-vue', bootstrap_servers='localhost:9092', enable_auto_commit=False)
print('Consomation d\energie par region')
for msg in consommateur:
    production = json.loads(msg.value)
    mem[msg.key.decode()] = production["consumption"]
    print('--------------------------------------')
    # print(production["date"]) 
    print(f'Update de la région : {production["name"]} ({msg.key.decode()})')
    print(mem)
    consommateur.commit()
