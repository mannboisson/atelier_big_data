import json
import matplotlib
matplotlib.use('TkAgg')  # Utilisez le backend TkAgg pour l'affichage interactif
from kafka import KafkaConsumer
import matplotlib.pyplot as plt
from collections import deque

# Initialisation de la file pour stocker les 15 dernières valeurs
history = {c: {'values': deque(maxlen=15), 'dates': deque(maxlen=15)} for c in ['Thermique', 'Nucléaire', 'Eolien', 'Solaire', 'Hydraulique', 'Bioénergies']}

# Initialisation de la figure Matplotlib
plt.ion()  # Mode interactif pour mettre à jour la figure en temps réel
fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(10, 8))
fig.subplots_adjust(hspace=0.555) 
axs = axs.flatten()

consommateur = KafkaConsumer('type_energie', group_id='type-energie-vue', bootstrap_servers='localhost:9092', enable_auto_commit=False)

for msg in consommateur:
    production = json.loads(msg.value)
    region = msg.key.decode()

    # Accumuler la production pour chaque type d'énergie
    history[region]['values'].append(production["value"])
    history[region]['dates'].append(production["date"])

    # Afficher les courbes individuelles
    for i, (region, data) in enumerate(history.items()):
        axs[i].clear()
        axs[i].plot(data['values'], label=region)
        axs[i].set_title(f"Évolution de la production d'énergie - {region}")
        axs[i].set_xlabel("Derniers enregistrements")
        axs[i].set_ylabel("Production d'énergie")
        axs[i].legend()

    # Afficher le résumé
    axs[-1].clear()
    for region, data in history.items():
        axs[-1].plot(data['values'], label=region)
    axs[-1].set_title("Résumé de la production d'énergie")
    axs[-1].set_xlabel("Derniers enregistrements")
    axs[-1].set_ylabel("Production d'énergie")
    axs[-1].legend()

    plt.pause(0.1)  # Pause pour permettre à la figure de se mettre à jour

    consommateur.commit()
