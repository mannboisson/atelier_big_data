import json
from kafka import KafkaConsumer

# Initialisation du dictionnaire pour stocker la somme totale par type d'énergie
mem = {c: 0 for c in ['Thermique', 'Nucléaire', 'Eolien', 'Solaire', 'Hydraulique', 'Bioénergies']}

consommateur = KafkaConsumer('type_energie', group_id='type-energie-vue', bootstrap_servers='localhost:9092', enable_auto_commit=False)

for msg in consommateur:
    production = json.loads(msg.value)
    
    # Accumuler la production pour chaque type d'énergie
    mem[msg.key.decode()] += production["value"]
    
    print('--------------------------------------')
    print(production["date"])
    print(f'Update de l\'énergie : {msg.key.decode()}')
    print(mem)
    
    consommateur.commit()
